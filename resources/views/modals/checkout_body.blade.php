<form id="purchaseForm" action="index.html" method="post">
  <input type="hidden" name="shipping_to" value="">
  <input type="hidden" name="order[shopper_id]" value="{{$shopper_id}}">
  <input type="hidden" name="order[article_id]" value="{{$product->article_id}}">
  <input type="hidden" name="address[address_json]" value="">
  <div class="tab-content">
    <!--STEP 1-->
    <div class="tab-pane active" id="pt-modal-step-1" role="tabpanel" aria-labelledby="home-tab">
      <div class="row">
        <div class="col-md-7">
          <div id="productCheckOutCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              @foreach($product->photos as $index => $photo)
              <li data-target="#productCheckOutCarousel" data-slide-to="{{$index}}" class="@if($index == 0) active @endif"></li>
              @endforeach
            </ol>
            <div class="carousel-inner">
              @foreach($product->photos as $index => $photo)
              <div class="carousel-item @if($index == 0) active @endif">
                <img src="{{$photo->path}}" class="d-block w-100" alt="...">
              </div>
              @endforeach
            </div>
            <a class="carousel-control-prev" href="#productCheckOutCarousel" role="button" data-slide="prev">
              <i class="fas fa-angle-left"></i>
            </a>
            <a class="carousel-control-next" href="#productCheckOutCarousel" role="button" data-slide="next">
              <i class="fas fa-angle-right"></i>
            </a>
          </div>
        </div>
        <div class="col-md-5">
          <div class="checkOutModalInfo">
            <div class="text-center p-2">

              <h2>{{$product->brand->brand_name}}</h2>
              <p>{{$product->description}}</p>
              @if(count($product->best_price)>0)
              <p>{{$currency}} {{number_format($product->best_price[0]->value,2)}}</p>

              <div class="form-group">
                <select class="form-control text-center" name="order[size_id]">
                  @foreach($availabilities as $av)
                      <option value="{{$av->size_id}}">Size {{$av->value}}</option>
                  @endforeach
                </select>
              </div>
              <a class="btn btn-secondary" id="messages-tab" data-toggle="tab" href="#pt-modal-step-2" role="tab" aria-controls="messages" aria-selected="false">Add to bag</a>
              @endif
            </div>
          </div>

        </div>
      </div>
    </div>
    <!--STEP 2-->
    <div class="tab-pane" id="pt-modal-step-2" role="tabpanel" aria-labelledby="profile-tab">
      <div class="row">

        <div class="col-md-4 col-sm-12 text-center">
          <h4>Summary</h4>
          <div class="check-out-summary-image-wrapper">
              <img class="img-fluid" src="{{$product->photos[0]->path}}" alt="">
          </div>

          <h2>{{$product->brand->brand_name}}</h2>
          <p class="desc">{{$product->description}}</p>
          @if(count($product->best_price)>0)
          <p class="price">{{$currency}} {{number_format($product->best_price[0]->value,2)}}</p>
          @endif
          <div id="shippingCost" class="d-none">

          </div>
        </div>
        <div class="col-md-4 col-sm-12">
          <h4>Shipping</h4>
          <div class="form-group ">
            <div class="row">
              <div class="col-md-6">
                <input name="address[address_first_name]" type="text" class="form-control" placeholder="Name*">
              </div>
              <div class="col-md-6">
                <input name="address[address_last_name]" type="text" class="form-control" placeholder="Surname*">
              </div>
            </div>
          </div>
          <div class="form-group d-none hiddenShippingField">
            <input id="addressInput" name="address[full_address]" type="text" class="form-control" placeholder="Shipping Address*">
          </div>
          <div class="form-group d-none hiddenShippingField">
            <input name="address[address_telephone]" type="text" class="form-control" placeholder="Telephone: +3902123456*">
          </div>
          <div class="form-group d-none hiddenShippingField">
            <textarea name="order[notes]" class="form-control" placeholder="Notes"></textarea>
          </div>
          <div class="form-group alert alert-secondary">
            <label><input name="order[shipping_method]" type="radio" placeholder="" value="contract"> I want to deal with the shopper</label>
            <p class="text-muted small">Get in touch with the shopper to finalize the order.</p>
          </div>
          <div class="form-group alert alert-secondary">
            <label><input name="order[shipping_method]" type="radio" placeholder="" value="direct"> I Want Direct Shipping</label>
            <p class="text-muted small">The boutique will ship the order directly to your desider address, it will involve costs.</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-12">

          <h4>Payment</h4>
          <div class="form-row mb-2">
            <div class="col">
              <label>Email <span class="required">*</span></label>
              <input name="email" type="email" class="form-control" placeholder="info@example.com" required="">
            </div>
          </div>
          <div class="form-row mb-2">
            <div class="col">
              <label>Credit card number <span class="required">*</span></label>
              <input name="card[num]" type="text" class="form-control" placeholder="ex. 1234 1234 1234 1234" required="">
            </div>
          </div>
          <div class="form-row mb-2">
            <div class="col">
              <label>Card holder name <span class="required">*</span></label>
              <input name="card[name]" type="text" class="form-control" placeholder="Card Holder Name" required="">
            </div>
          </div>
          <div class="form-row mb-2">
            <div class="col">
              <label>Exp. date <span class="required">*</span></label>
              <div class="form-row">
                <div class="col"><input name="card[mm]" type="text" class="form-control" placeholder="MM" required=""></div>
                <div class="col"><input name="card[yy]" type="text" class="form-control" placeholder="YY" required=""></div>
              </div>
            </div>
            <div class="col">
              <label>Security code <span class="required">*</span></label>
              <input name="card[ccv]" type="text" class="form-control" placeholder="• • •" required="">
            </div>
          </div>

          <button class="btn btn-secondary btn-block btn-lg" type="submit" id="confirm-purchase-btn">CONFIRM</button>
          <hr>
          <a class="btn btn-secondary btn-sm" id="settings-tab" data-toggle="tab" href="#pt-modal-step-1" role="tab" aria-controls="settings" aria-selected="false">BACK</a>
        </div>
      </div>

    </div>
    <!--STEP 3-->
  </div>

</form>
