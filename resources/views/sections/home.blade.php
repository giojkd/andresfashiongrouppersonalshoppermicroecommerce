<style media="screen">
.shopper-cover{
  height: 320px;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;
}
.shopper-board{
  margin-top: -120px;

  background: rgba(255,255,255,0.6);
  padding: 18px;
}
</style>
<div class="shopper-cover" style="background-image:url('http://www.andresfashiongroup.com/{{$shopper->cover_image}}')"></div>
<div class="shopper-board container">
    <div class="row">
      <div class="col-md-4 text-center">
        <div class="shopper-avatar-wrapper">
          <div class="shopper-avatar">
            <img src="http://www.andresfashiongroup.com/{{$shopper->avatar_image}}" alt="">
          </div>
        </div>
      </div>
      <div class="col-md-6 ">
        <h1 class="shopper-nickname">{{$shopper->shopper_nickname}}</h1>
        <div class="row shopper-info">
          <div class="col-md-4">
            <b>{{count($products)}}</b> products
          </div>
          <div class="col-md-4">
            <b>{{$count_sales}}</b> sales
          </div>
          <div class="col-md-4">
            <b>{{$count_followers}}</b> followers
          </div>
        </div>
        <p class="shopper-description">{{$shopper->shopper_description}}</p>
      </div>
    </div>

</div>
<div class="container">
  <div class="row">
    <div class="d-none">
      <pre>
        <?php print_r($products)?>
      </pre>
    </div>
    <!-- Set all conditions to include product thumbnail -->
    @foreach($products as $product)
    @if(!is_null($product->brand))
    @include('sections.product_thumb')
    @endif
    @endforeach
  </div>
</div>
