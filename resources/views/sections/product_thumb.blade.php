<div class="col-md-4 pt">
  <a href="javascript:" onclick="api.openCheckoutModal({{$product->article_id}})">
    <div class="pt-header">

    </div>
    <div class="pt-body">
      <div class="pt-body-overlay">
        <div class="text-center">
          <h2>{{$product->brand->brand_name}}</h2>
          <p class="desc">{{$product->description}}</p>
          @if(count($product->best_price)>0)
          <p class="price">{{$currency}} {{number_format($product->best_price[0]->value,2)}}</p>
          @endif
        </div>

      </div>
      <div class="pt-body-image-wrapper">
        <img src="{{$product->photos[0]->path}}" alt="">
      </div>
    </div>
    <div class="pt-footer">

    </div>
  </a>
</div>
