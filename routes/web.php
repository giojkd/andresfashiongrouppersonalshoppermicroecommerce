<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{user_slug}', 'Shopper@index');
Route::get('/modal-checkout/{article_id}/{shopper_id}','Shopper@modalCheckOutBody');
Route::get('/thank-you/{order_id}','Shopper@thankYou');
