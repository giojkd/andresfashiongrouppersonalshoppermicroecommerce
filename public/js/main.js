var apiPrefix = '/api/';
var address = {};
var product_id = 0;
var shippingCost = 0;
var addressSelected = 0;
var api = {
  openCheckoutModal:function(id){
    product_id = id;
    $('#checkOutModal .modal-body').load('/modal-checkout/'+id+'/'+shopper_id,function(){
      checkOutModalLoaded();
    });
    $('#checkOutModal').modal('show');
  },
  calculateShippingCost:function(){
    if($('input[name="order[shipping_method]"]:checked').val() == 'contract'){
        $('#confirm-purchase-btn').attr('disabled',false);
    }
    if(addressSelected == 1){
      console.log(address);
      switch($('input[name="order[shipping_method]"]:checked').val()){
        case 'contract':
        $('#shippingCost').addClass('d-none');
        break;
        case 'direct':
        $('input[name="shipping_to"]').val(address.suggestion.countryCode)
        axios
        .post(apiPrefix+'shipping-cost', {
          crossDomain: true,
          product_id:product_id,
          shipping_to:address.suggestion.countryCode
        })
        .then(response => {
          if(response.data.cost == -1){
            $('#confirm-purchase-btn').attr('disabled',true);
            $('#shippingCost').html('We don\'t ship in your country.');
            $('#shippingCost').removeClass('d-none');
          }else{
            shippingCost = (parseFloat(response.data.cost) >0 ) ? '&euro;'+parseFloat(response.data.cost).toFixed(2) : ' free';
            var shippingCostString =
            $('#shippingCost').html('Shipping cost:  '+shippingCost+' from '+response.data.it_lang);
            $('#shippingCost').removeClass('d-none');
          }
        })
        break;
      }
    }
  },
  confirmPurchase:function(){
    axios
    .post(apiPrefix+'confirm-purchase',$('#purchaseForm').serialize())
    .then(response => {
      console.log(response.data);
      if(response.data.status == 1){
        window.location.href = '/thank-you/'+response.data.order.id;
      }else {
        console.log(reponse.data.error);
      }
    })
  }
}




function checkOutModalLoaded(){
  $('#purchaseForm').submit(function(e){
    e.preventDefault();
    api.confirmPurchase();
  })
  var placesAutocomplete = places({
    appId: 'plY38PWBPT7W',
    apiKey: 'adfbf2866995fccd1c217981f93e8c12',
    container: document.querySelector('#addressInput'),
  }).configure({
    type: 'address'
  });
  placesAutocomplete.on('change', function resultSelected(e) {
    address = e;
    $('input[name="address[address_json]"]').val(JSON.stringify(e.suggestion));
    addressSelected = 1;
    api.calculateShippingCost();
  });

  $('input[name="order[shipping_method]"]').change(function(){
    $('.hiddenShippingField').removeClass('d-none');
    api.calculateShippingCost();
  })
}

$(function(){
  if(article_id > 0){
    api.openCheckoutModal(article_id);
  }
})
