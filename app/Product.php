<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{
  protected $table = 'articles';
  protected $primaryKey = 'article_id';

  public function category(){
    return $this->belongsTo('\App\Category','category_id','id');
  }

  public function photos()
  {
    return $this->hasMany('App\Photo','article_id', 'article_id');
  }

  public function prices()
  {
    return $this->hasMany('App\Price','article_id', 'article_id');
  }

  public function best_price()
  {
    return $this->hasMany('App\Price','article_id', 'article_id')
    ->where('price_type_id',2)
    ->where('value','>',0)
    ->orderBy('value','ASC')
    ->limit(1);
  }

/*  public function availabilities(){
    return $this->hasMany('App\Availability','article_id','article_id');
  } */

  public function availabilities(){

    $article_id = $this->article_id;

    return DB::select( DB::raw("SELECT a.size_id, SUM(a.quantity) as quantity, svs.value, sv.scale_variation_id, sv.scale_variation_name, s.scale_name  FROM availabilities a LEFT JOIN scales_variations_values svs ON a.size_id = svs.value_id LEFT JOIN scales_variations sv ON svs.scale_variation_id = sv.scale_variation_id LEFT JOIN scales s ON sv.scale_id = s.scale_id WHERE a.article_id = :article_id AND sv.default_variation = 1 GROUP BY a.size_id, svs.value, sv.scale_variation_id,  sv.scale_variation_name, s.scale_name"), array(
      'article_id' => $article_id,
    ));
  }

  public function brand(){
    return $this->belongsTo('App\Brand','brand_id','brand_id');
  }
}
