<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Shopper extends Model
{
    protected $primaryKey = 'shopper_id';

    public function providers(){
       $providers = DB::table('providers_shoppers as ps')
       ->leftJoin('providers as p','p.provider_id','=','ps.provider_id')
       ->where('ps.shopper_id',$this->shopper_id)
       ->get();
       return $providers;
    }
}
