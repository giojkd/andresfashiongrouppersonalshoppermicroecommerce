<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cartalyst\Stripe\Stripe;

class Shopper extends Controller
{
  public function index($user_slug){
    $user_slug = trim(strtolower($user_slug));
    $shopper = \App\Shopper::where('shopper_nickname',$user_slug)->first();
    if(is_null($shopper)){
      abort(404);
    }
    $products =
    \App\Product::with(['photos','brand','best_price'])
    ->leftJoin('articles_shoppers as arsh','arsh.article_id','=','articles.article_id')
    ->where('arsh.shopper_id',$shopper->shopper_id)
    ->where('arsh.status',1)
    ->get();
    $data['shopper'] = $shopper;

    $data['products'] = $products;
    $data['currency'] = 'EUR';
    $data['count_sales'] = 623;
    $data['count_followers'] = 1932;
    return view('main',$data);

  }
  public function modalCheckOutBody($id,$shopper_id){
    $product = \App\Product::with([
      'photos',
      'brand',
      'best_price',
      ])->find($id);

      $data = [
        'product' => $product,
        'availabilities' => $product->availabilities(),
        'shopper_id' => $shopper_id
      ];
      $data['currency'] = 'EUR';
      return view('modals.checkout_body',$data);
    }

    public function calculateShippingCostByArticleAndCountryOfDestination($article_id,$country){
      $query = "SELECT bsc.cost, sc1.it_lang
      FROM box_shipping_cost bsc
      INNER JOIN boxes b ON bsc.box_id = b.id
      INNER JOIN boxes_categories bc ON bc.box_id = b.id
      INNER JOIN articles a ON bc.category_id = a.category_id
      INNER JOIN articles_providers ap ON ap.article_id = a.article_id
      INNER JOIN providers p ON p.provider_id = ap.provider_id
      INNER JOIN store_country sc ON bsc.country_to_id = sc.id
      INNER JOIN store_country sc1 ON p.store_country_id = sc1.id_country
      WHERE a.article_id = ".$article_id." AND sc.iso_code LIKE '".$country."'
      ORDER BY bsc.cost ASC LIMIT 1";

      $results = DB::select(DB::raw($query));
      if(count($results)>0){
        return (array)$results[0];
      }else{
        return ['cost'=>-1];
      }
      return $results;
    }

    public function getShippingCostByCategory(Request $request){
      $data = $request->all();
      return $this->calculateShippingCostByArticleAndCountryOfDestination($data['product_id'],$data['shipping_to']);
    }

    public function thankYou($id){


      $data['order'] = \App\Shopperorder::find($id);

      return view('thankyou',$data);
    }

    public function confirmPurchase(Request $request){

      $data = $request->all();



      $card = $data['card'];

      $shopper = \App\Shopper::find($data['order']['shopper_id']);


      $shopperProviders = $shopper->providers();
      $assignedProvider = $shopperProviders[0];

      $data['order']['provider_id'] = $assignedProvider->provider_id;

      $shippingCostValue = 0;
      if($data['order']['shipping_method'] == 'direct'){
        $shoppingCost =  $this->calculateShippingCostByArticleAndCountryOfDestination($data['order']['article_id'],$data['shipping_to']);
        $shippingCostValue = $shoppingCost['cost'];

      }

      $product = \App\Product::with([
        'photos',
        'brand',
        'best_price',
        ])->find($data['order']['article_id']);

        $stripe = new Stripe('sk_test_pG6VIpNXj57SoiwwS9aUnZVK00xkTrDNgS', '2019-05-16');


        $total = $product->best_price[0]->value+$shippingCostValue;

        $data['order']['total'] = $total;

        try{
          $token = $stripe->tokens()->create(
            array(
              "card" => array(
                "name" => $card['name'],
                "number" => $card['num'],
                "exp_month" => $card['mm'],
                "exp_year" => $card['yy'],
                "cvc" => $card['ccv']
              )
            )
          );

          $customer =  $stripe->customers()->create([
            'source' => $token['id'],
            'email' => $data['email']
          ]);

          $charge = $stripe->charges()->create([
            "amount" => round($total*100) ,
            "currency" => "eur",
            "customer" => $customer['id'],
            "description" => "AFGPS Order  ".$data['email'],
          ]);

          if($charge['status']=='succeeded'){
            $address = \App\Shopperaddress::create($data['address']);
            $data['order']['shopperaddress_id'] = $address->id;
            $order = \App\Shopperorder::create($data['order']);
            return ['status'=>1,'order'=>$order];

          }

        }catch(Cartalyst\Stripe\Exception\CardErrorException $e) {

          $code = $e->getCode();

          $message = $e->getMessage();

          $type = $e->getErrorType();
          return ['status' => 0,'error'=>$error];

        }
      }
    }
