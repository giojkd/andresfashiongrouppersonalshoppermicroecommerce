<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scale extends Model
{
  protected $table = 'scales';
  protected $primaryKey = 'scale_id';

  public function values()
  {
    return $this->hasMany('App\ScaleValue','scale_id', 'scale_id');
  }

  public function variations()
  {
    return $this->hasMany('App\ScaleVariation','scale_id', 'scale_id');
  }
}
