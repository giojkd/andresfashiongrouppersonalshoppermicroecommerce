<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Availability extends Model
{

  public function size()
  {
    return $this->hasMany('App\ScaleVariationValue','value_id', 'size_id');
  }

}
