<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScaleVariationValue extends Model
{
  protected $table = 'scales_variations_values';
  protected $primaryKey = 'scale_variation_value_id';


  public function scale_variation()
  {
    return $this->belongsTo('App\ScaleVariation','scale_variation_id', 'scale_variation_id');
  }
}
