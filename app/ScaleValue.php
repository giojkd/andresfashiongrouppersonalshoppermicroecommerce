<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScaleValue extends Model
{
  protected $table = 'scales_values';
  protected $primaryKey = 'value_id';

  public function scale()
  {
    return $this->belongsTo('App\Scale','scale_id', 'scale_id');
  }
}
