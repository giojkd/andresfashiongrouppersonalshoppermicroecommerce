<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
  protected $table = 'articles_photo';
  protected $primaryKey = 'id_photo';

  public function getPathAttribute($value)
  {
    return 'http://www.andresfashiongroup.com/'.$value;
  }

}
