<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScaleVariation extends Model
{
  protected $table = 'scales_variations';
  protected $primaryKey = 'scale_variation_id';

  public function scale()
  {
    return $this->belongsTo('App\Scale','scale_id', 'scale_id');
  }

  public function values(){
    return $this->hasMany('App\ScaleVariationValue','scale_variation_id', 'scale_variation_id');
  }

  
}
